<?php
session_start();
include '../db.php';

// Check if form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Check if username and password are provided
    if (!empty($_POST['username']) && !empty($_POST['password'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];

        // Prepare a SQL statement to fetch the user with the provided username
        $stmt = $pdo->prepare("SELECT * FROM users WHERE username = ?");
        $stmt->execute([$username]);
        $user = $stmt->fetch();

        if ($user && password_verify($password, $user['password'])) {
            // Authentication successful, set session variables
            $_SESSION['user_id'] = $user['id']; // Store the user ID in the session
            $_SESSION['username'] = $user['username'];
            // Redirect to the dashboard or another page
            header('Location: ../blogs');
            exit();
        } else {
            // Authentication failed, redirect back to login page with error message
            header('Location: ../login');
            exit();
        }
    } else {
        // Username or password not provided, redirect back to login page with error message
        header('Location: ../login');
        exit();
    }
} else {
    // Redirect to login page if accessed directly
    header('Location: ../login');
    exit();
}
?>
