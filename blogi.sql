-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2024 at 11:27 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blogi`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `commenter` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `commenter`, `content`, `date_created`) VALUES
(2, 10, 'admin', 'hi', '2024-02-12 07:39:02'),
(3, 17, 'bad', 'hi', '2024-02-12 08:08:03');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `comments_enabled` tinyint(1) NOT NULL DEFAULT 1,
  `scheduled_publish` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `author`, `date_created`, `comments_enabled`, `scheduled_publish`) VALUES
(1, 'First', 'Blog', 'Here', '0000-00-00 00:00:00', 1, NULL),
(2, 'test', 'test', 'hege', '2024-02-07 08:18:55', 1, NULL),
(3, 'testi testi', 'tei', 'hege', '2024-02-06 22:00:00', 1, NULL),
(4, 'aaaaaaaa', 'aaaaaaaaa', 'hege', '2024-02-06 22:00:00', 1, NULL),
(5, 'hhrhr', 'hrhrhr', 'hege', '2024-02-06 22:00:00', 1, NULL),
(6, 'aaaa', 'aaaa', 'hege', '2024-02-07 07:15:13', 1, NULL),
(7, 'wsg', 'wsg', 'testaccount', '2024-02-07 07:24:40', 1, NULL),
(8, 'test', 'keep yourself safe', 'admin', '2024-02-09 08:33:32', 1, NULL),
(10, 'aa', 'aa', 'admin', '2024-02-09 08:39:30', 1, NULL),
(11, 'tes', 'test', 'admin', '2024-02-09 08:48:14', 0, NULL),
(15, 'aa', 'aa', 'admin', '2024-02-12 07:47:57', 1, '2024-02-16 09:47:00'),
(16, 'hii', 'test', 'admin', '2024-02-12 07:53:03', 1, '2024-02-13 10:00:00'),
(17, 'test', '1', 'admin', '2024-02-12 07:59:05', 1, '2024-02-12 10:00:00'),
(19, 'post title', 'post content', 'admin', '2024-02-12 09:06:11', 1, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
