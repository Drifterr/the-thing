<?php
session_start();

// Check if the user is logged in
if (!isset($_SESSION['username'])) {
    header('Location: ../login');
    exit();
}

// Connect to the database
include '../db.php';

// Handle form submissions for changing username
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['change_username'])) {
    $newUsername = $_POST['new_username'];

    // Update the username in the database
    $stmt = $pdo->prepare("UPDATE users SET username = ? WHERE id = ?");
    $stmt->execute([$newUsername, $_SESSION['user_id']]);

    // Update the session variable with the new username
    $_SESSION['username'] = $newUsername;

    // Redirect to the user panel
    header('Location: ' . $_SERVER['PHP_SELF']);
    exit();
}

// Handle form submissions for changing password
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['change_password'])) {
    $newPassword = $_POST['new_password'];

    // Update the password in the database
    $hashedPassword = password_hash($newPassword, PASSWORD_DEFAULT);
    $stmt = $pdo->prepare("UPDATE users SET password = ? WHERE id = ?");
    $stmt->execute([$hashedPassword, $_SESSION['user_id']]);

    // Redirect to the user panel
    header('Location: ' . $_SERVER['PHP_SELF']);
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Panel</title>
    <link rel="stylesheet" href="../assets/css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico"/>
</head>
<body>
<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="../blogs">Home</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="mynavbar">
      <ul class="navbar-nav me-auto">
        <li class="nav-item">
          <a class="nav-link" href="#">User Panel</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../logout">Log out</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<main>
    <h2>User Panel - <?php echo htmlspecialchars($_SESSION['username'], ENT_QUOTES, 'UTF-8'); ?></h2>
    
    <!-- Form for changing username -->
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        <label for="new_username">New Username:</label>
        <input placeholder="New username" type="text" id="new_username" name="new_username" required>
        <button type="submit" name="change_username">Change Username</button>
    </form>

    <br>
    
    <!-- Form for changing password -->
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        <label for="new_password">New Password:</label>
        <input placeholder="New password" type="password" id="new_password" name="new_password" required>
        <button type="submit" name="change_password">Change Password</button>
    </form>
    <main>
</body>
</html>
