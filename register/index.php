<?php
session_start();

// If user is already logged in, redirect them to the dashboard
if(isset($_SESSION['username'])) {
    header('Location: ../blogs');
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Register</title>
    <link rel="stylesheet" href="../assets/css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico"/>
  </head>
  <body>
  <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="../blogs">Home</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="mynavbar">
      <ul class="navbar-nav me-auto">
        <li class="nav-item">
          <a class="nav-link" href="../user_panel">User Panel</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../logout">Log out</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<main>
    <div class="register-container">
      <h2>Register</h2>
      <form action="../register_process/" method="post">
        <label for="username">Username:</label>
        <input placeholder="Username" type="text" id="username" name="username" required /><br />
        <label for="password">Password:</label>
        <input placeholder="Password" type="password" id="password" name="password" required /><br />
        <label for="confirm_password">Confirm Password:</label>
        <input
          placeholder="Confirm Password"
          type="password"
          id="confirm_password"
          name="confirm_password"
          required
        /><br />
        <button type="submit">Register</button>
      </form>
      <p>Already have an account? <a href="../login/">Login</a></p>
    </div>
</main>
  </body>
</html>
