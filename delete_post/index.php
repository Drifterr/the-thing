<?php
session_start();

// If user is not logged in, redirect them to the login page
if (!isset($_SESSION['username'])) {
    header('Location: ../login');
    exit();
}

// Include your database connection file
$pdo = new PDO('mysql:host=localhost;dbname=blogi', 'root');

// Check if post ID is provided in the URL
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    // Fetch the post from the database
    $stmt = $pdo->prepare("SELECT * FROM posts WHERE id = ?");
    $stmt->execute([$id]);
    $post = $stmt->fetch(PDO::FETCH_ASSOC);

    // Check if the logged-in user is the author of the post
    if ($post && $post['author'] === $_SESSION['username']) {
        // Delete the post from the database
        $stmt = $pdo->prepare("DELETE FROM posts WHERE id = ?");
        $stmt->execute([$id]);
    }
}

// Redirect back to the blog page after deletion
header('Location: ../blogs');
exit();
?>
