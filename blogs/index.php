<?php
session_start();

// If user is not logged in, redirect them to the login page
if (!isset($_SESSION['username'])) {
    header('Location: ../login');
    exit();
}
// Connect to your MySQL database
$pdo = new PDO('mysql:host=localhost;dbname=blogi', 'root');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!empty($_POST['title']) && !empty($_POST['content'])) {
        $title = $_POST['title'];
        $content = $_POST['content'];
        $author = $_SESSION['username'];
        $scheduled_publish = isset($_POST['scheduled_publish']) ? $_POST['scheduled_publish'] : null; // Get scheduled publish date and time

        $stmt = $pdo->prepare("INSERT INTO posts (title, content, author, scheduled_publish) VALUES (?, ?, ?, ?)");
        $stmt->execute([$title, $content, $author, $scheduled_publish]);

        // Redirect to the current page to prevent form resubmission on refresh
        header('Location: ' . $_SERVER['PHP_SELF']);
        exit();
    }
}

// Fetch blog posts from the database
$statement = $pdo->query("SELECT * FROM posts ORDER BY date_created DESC");
$posts = $statement->fetchAll(PDO::FETCH_ASSOC);

// Count the total number of blog posts
$totalPosts = $pdo->query("SELECT COUNT(*) FROM posts")->fetchColumn();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HRBlog</title>
    <link rel="stylesheet" href="../assets/css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico"/>
</head>
<body>
<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Home</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="mynavbar">
      <ul class="navbar-nav me-auto">
        <li class="nav-item">
          <a class="nav-link" href="../user_panel">User Panel</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../logout">Log out</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<main>
    <h2>Welcome, <?php echo htmlspecialchars($_SESSION['username']); ?>!</h2>
    
        <!-- Blog post form -->
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" class="blogform">
            <label for="title">Title:</label><br>
            <input placeholder="Title" type="text" id="title" name="title" required><br>
            <label for="content">Content:</label><br>
            <textarea placeholder="Content" id="content" name="content" rows="4" required></textarea><br>
            <label for="scheduled_publish">Scheduled Publish Date and Time:</label><br>
            <input type="datetime-local" id="scheduled_publish" name="scheduled_publish"><br><br> <!-- Allow users to specify date and time -->
            <button type="submit">Add Blog Post</button>
        </form>
        <p class="totalposts">Total Blog Posts: <?php echo $totalPosts; ?></p> <!-- Display total number of blog posts -->
    
        <section>
        <?php foreach ($posts as $post): ?>
    <article>
        <h2><?php echo htmlspecialchars($post['title']); ?></h2>
        <p><?php echo htmlspecialchars($post['content']); ?></p>
        <?php if ($post['scheduled_publish'] && strtotime($post['scheduled_publish']) > time()): ?>
            <p>Not yet published. Scheduled for <?php echo $post['scheduled_publish']; ?></p>
        <?php else: ?>
            <p>Published on <?php echo $post['date_created']; ?> by <?php echo htmlspecialchars($post['author']); ?></p>
        <?php endif; ?>
        <?php if ($post['author'] === $_SESSION['username']): ?>
            <a href="../edit_post/index.php?id=<?php echo htmlspecialchars($post['id']); ?>" class="edit-link">Edit</a>
            <a href="../delete_post/index.php?id=<?php echo htmlspecialchars($post['id']); ?>" class="delete-link">Delete</a>
        <?php endif; ?>
        <?php if (!$post['scheduled_publish'] || strtotime($post['scheduled_publish']) <= time()): ?>
            <?php if ($post['comments_enabled']): ?>
                <form action="../add_comment/index.php" method="post">
                    <input type="hidden" name="post_id" value="<?php echo $post['id']; ?>">
                    <label for="comment">Your Comment:</label><br>
                    <textarea placeholder="Comment" id="comment" name="comment" rows="4" required></textarea><br>
                    <button type="submit">Submit Comment</button>
                </form>
            <?php else: ?>
                <p>Comments are disabled for this post.</p>
            <?php endif; ?>
            <?php 
            if ($post['comments_enabled']) { // Only fetch and display comments if comments are enabled
                // Fetch comments associated with the current post
                $stmt = $pdo->prepare("SELECT * FROM comments WHERE post_id = ?");
                $stmt->execute([$post['id']]);
                $comments = $stmt->fetchAll(PDO::FETCH_ASSOC);

                // Display comments
                foreach ($comments as $comment) {
                    echo "<div class='comment'>";
                    echo "<p><strong>" . htmlspecialchars($comment['commenter']) . "</strong> said:</p>";
                    echo "<p>" . htmlspecialchars($comment['content']) . "</p>";
                    echo "<p>Posted on " . htmlspecialchars($comment['date_created']) . "</p>";
                    // Add edit and delete buttons if the commenter is the logged-in user
                    if ($comment['commenter'] === $_SESSION['username']) {
                        echo "<a href='../edit_comment/index.php?id={$comment['id']}' class='edit-comment'>Edit</a> ";
                        echo "<a href='../delete_comment/index.php?id={$comment['id']}' class='delete-comment'>Delete</a>";
                    }
                    echo "</div>";
                }                
            }
            ?>
        <?php endif; ?>
    </article>
<?php endforeach; ?>
        </section>
    </main>
</body>
</html>
