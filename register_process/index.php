<?php
session_start();
include '../db.php';

// Check if form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Check if username, password, and confirm password are provided
    if (!empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['confirm_password'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $confirm_password = $_POST['confirm_password'];

        // Check if passwords match
        if ($password !== $confirm_password) {
            header('Location: ../register?invalid-password');
            exit();
        }

        // Check if username is already taken
        $stmt = $pdo->prepare("SELECT * FROM users WHERE username = ?");
        $stmt->execute([$username]);
        if ($stmt->fetch()) {
            header('Location: ../register');
            exit();
        }

        // Hash the password before storing it in the database
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);

        // Insert the new user into the database
        $stmt = $pdo->prepare("INSERT INTO users (username, password) VALUES (?, ?)");
        $stmt->execute([$username, $hashed_password]);

        // Redirect to login page after successful registration
        header('Location: ../blogs');
        exit();
    } else {
        // Username, password, or confirm password not provided
        header('Location: ../register?username-or-password-or-confirm-password-not-provided');
        exit();
    }
} else {
    // Redirect to register page if accessed directly
    header('Location: ../register');
    exit();
}
?>
