<?php
session_start();

// If user is not logged in, redirect them to the login page
if (!isset($_SESSION['username'])) {
    header('Location: ../login');
    exit();
}

// Include your database connection file
$pdo = new PDO('mysql:host=localhost;dbname=blogi', 'root');

// Check if comment ID is provided in the URL
if (!isset($_GET['id'])) {
    // Redirect back to the blog page if no comment ID is provided
    header('Location: ../blogs');
    exit();
}

$id = $_GET['id'];

// Fetch the comment from the database
$stmt = $pdo->prepare("SELECT * FROM comments WHERE id = ?");
$stmt->execute([$id]);
$comment = $stmt->fetch(PDO::FETCH_ASSOC);

// Check if the logged-in user is the author of the comment
if (!$comment || $comment['commenter'] !== $_SESSION['username']) {
    // Redirect back to the blog page if the user is not the author of the comment
    header('Location: ../blogs');
    exit();
}

// Handle form submission to update the comment
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!empty($_POST['comment'])) {
        $newComment = $_POST['comment'];
        
        // Update the comment in the database
        $stmt = $pdo->prepare("UPDATE comments SET content = ? WHERE id = ?");
        $stmt->execute([$newComment, $id]);

        // Redirect back to the blog page after editing the comment
        header('Location: ../blogs');
        exit();
    } else {
        // Handle validation errors or missing fields
        $error = "Please fill in all fields.";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Comment</title>
    <link rel="stylesheet" href="../assets/css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico"/>
</head>
<body>
<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="../blogs">Home</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="mynavbar">
      <ul class="navbar-nav me-auto">
        <li class="nav-item">
          <a class="nav-link" href="../user_panel">User Panel</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../logout">Log out</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<main>
    <h2>Edit Comment</h2>
    <?php if (isset($error)): ?>
        <p><?php echo $error; ?></p>
    <?php endif; ?>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) . '?id=' . $id; ?>" method="post">
        <label for="comment">Edit Your Comment:</label><br>
        <textarea placeholder="Comment" id="comment" name="comment" rows="4" required><?php echo htmlspecialchars($comment['content']); ?></textarea><br>
        <button type="submit">Save Changes</button>
    </form>
    </main>
</body>
</html>
