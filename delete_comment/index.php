<?php
session_start();

if (!isset($_SESSION['username'])) {
    header('Location: ../login');
    exit();
}

// Include your database connection file
$pdo = new PDO('mysql:host=localhost;dbname=blogi', 'root');

// Check if comment ID is provided in the URL
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    // Fetch the comment from the database
    $stmt = $pdo->prepare("SELECT * FROM comments WHERE id = ?");
    $stmt->execute([$id]);
    $comment = $stmt->fetch(PDO::FETCH_ASSOC);

    // Check if the logged-in user is the author of the comment
    if ($comment && $comment['commenter'] === $_SESSION['username']) {
        // Delete the comment from the database
        $stmt = $pdo->prepare("DELETE FROM comments WHERE id = ?");
        $stmt->execute([$id]);
    }
}

// Redirect back to the blog page after deletion
header('Location: ../blogs');
exit();
?>
