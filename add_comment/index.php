<?php
session_start();

// If user is not logged in, redirect them to the login page
if (!isset($_SESSION['username'])) {
    header('../login');
    exit();
}

// Include your database connection file
$pdo = new PDO('mysql:host=localhost;dbname=blogi', 'root');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!empty($_POST['comment']) && isset($_POST['post_id'])) {
        $commenter = $_SESSION['username']; // Set commenter as the logged-in user
        $comment = $_POST['comment'];
        $post_id = $_POST['post_id'];

        $stmt = $pdo->prepare("INSERT INTO comments (post_id, commenter, content) VALUES (?, ?, ?)");
        $stmt->execute([$post_id, $commenter, $comment]);

        // Redirect back to the blog page after adding the comment
        header('Location: ../blogs');
        exit();
    } else {
        // Handle validation errors or missing fields
        echo "Please fill in all fields.";
    }
}
?>
