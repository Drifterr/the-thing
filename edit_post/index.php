<?php
session_start();

// If user is not logged in, redirect them to the login page
if (!isset($_SESSION['username'])) {
    header('Location: ../login');
    exit();
}

// Include your database connection file
$pdo = new PDO('mysql:host=localhost;dbname=blogi', 'root');

// Check if post ID is provided in the URL
if (!isset($_GET['id'])) {
    // Redirect back to the blog page if no post ID is provided
    header('Location: index.php');
    exit();
}

$id = $_GET['id'];

// Fetch the post from the database
$stmt = $pdo->prepare("SELECT * FROM posts WHERE id = ?");
$stmt->execute([$id]);
$post = $stmt->fetch(PDO::FETCH_ASSOC);

// Check if the logged-in user is the author of the post
if (!$post || $post['author'] !== $_SESSION['username']) {
    // Redirect back to the blog page if the user is not the author of the post
    header('Location: ../blogs');
    exit();
}

// Handle form submission to update the post
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!empty($_POST['title']) && !empty($_POST['content'])) {
        $newTitle = $_POST['title'];
        $newContent = $_POST['content'];
        $commentsEnabled = isset($_POST['comments_enabled']) ? 1 : 0;

        // Update the post in the database
        $stmt = $pdo->prepare("UPDATE posts SET title = ?, content = ?, comments_enabled = ? WHERE id = ?");
        $stmt->execute([$newTitle, $newContent, $commentsEnabled, $id]);

        // Redirect back to the blog page after editing the post
        header('Location: ../blogs');
        exit();
    } else {
        // Handle validation errors or missing fields
        $error = "Please fill in all fields.";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Post</title>
    <link rel="stylesheet" href="../assets/css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico"/>
</head>
<body>
<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="../blogs">Home</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="mynavbar">
      <ul class="navbar-nav me-auto">
        <li class="nav-item">
          <a class="nav-link" href="../user_panel">User Panel</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../logout">Log out</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<main>
    <h2>Edit Post</h2>
    <?php if (isset($error)): ?>
        <p><?php echo $error; ?></p>
    <?php endif; ?>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) . '?id=' . $id; ?>" method="post">
        <label for="title">Title:</label><br>
        <input placeholder="Title" type="text" id="title" name="title" value="<?php echo htmlspecialchars($post['title']); ?>" required><br>
        <label for="content">Content:</label><br>
        <textarea placeholder="Content" id="content" name="content" rows="4" required><?php echo htmlspecialchars($post['content']); ?></textarea><br>
        <input type="checkbox" id="comments_enabled" name="comments_enabled" <?php echo $post['comments_enabled'] ? 'checked' : ''; ?>>
        <label for="comments_enabled">Enable Comments</label><br>
        <button type="submit">Save Changes</button>
    </form>
    </main>
</body>
</html>
